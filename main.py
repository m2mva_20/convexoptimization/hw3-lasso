#!/bin/python

import sys

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import tqdm


def genLassoProblem(n=10,d=200,lamb=10):
    '''Generate randomly the data and the observations of a Lasso problem of given dimension
    INPUT : n,d,lambda
    OUPUT : X, Y, lambda'''
    X = rd.normal(10, 2, size=(n,d))
    Y = 1*rd.rand(n,1)
    return X, Y, lamb


def initProblem(X, Y, lamb):
    '''Compute the dual problem of the lasso problem as a classical QP problem
    INPUT : X, Y, lambda
    OUTPUT : Q, p, A, b'''
    print("Formatting the Lasso problem as a classical QP problem ... ")
    n = X.shape[0]
    d = X.shape[1]
    if Y.shape[0] != n:
        print("Error in the dimension of the problem, exiting program ...")
        exit() # Die here and debug
    p = Y
    Q = 1/2*np.eye(n)
    A = np.array([X.T, -X.T]).reshape((2*d,n))
    b = lamb*np.ones((2*d, 1))
    print("Succesfully formatting the problem !")
    return Q, p, A, b

def centering_step(Q, p, A, b, tB, v0, eps, alpha = 0.25, beta = 0.7):
    '''Compute the centering step thanks to newton algorithm and a backtracking line search'''
    m   = b.shape[0]
    n   = Q.shape[0]
    f   = lambda x : tB*(x.T.dot(Q).dot(x)[0,0] + p.T.dot(x)[0,0]) - np.sum(np.log(b-A.dot(x)))
    df  = lambda u : tB*(2*Q.dot(u) + p) + np.sum([1./(b[i]-A.dot(u)[i])*(A.T[:,i]) for i in range(m)], axis=0).reshape(n,1)
    ddf = lambda x : tB*2*Q + np.sum([1/(b-A.dot(x))[i]**2*A[i].reshape(n,1).dot(A.T[:,i].reshape(1, n)) for i in range(m)], axis=0)
    dec = 1
    x   = v0
    v   = [np.copy(x)]
    while dec/2 > eps:
        cdf  = df(x)
        cddf = ddf(x)
        dx  = -np.linalg.inv(cddf).dot(cdf)
        dec = cdf.T.dot(-dx)
        t=1
        while t>1e-12 and ( (b-A.dot(x+t*dx)<0).any() or  f(x+t*dx) >= f(x)+alpha*t*cdf.T.dot(dx)):
            t = beta*t
        x += t*dx
        dec = df(x).T.dot(np.linalg.inv(ddf(x)).dot(df(x)))
        v.append(np.copy(x))
    return v, dec



def barr_method(Q, p, A, b, v0, eps, mu=2):
    '''Compute the log barrier method to solve a QP problem'''
    t  = 1
    m  = b.shape[0]
    x, d  = centering_step(Q, p, A, b, t, v0, eps)
    X = [np.copy(x[-1])]
    T, D = [len(x)], [d[0,0]]
    while m/t >= eps:
        t = mu*t
        x, d = centering_step(Q, p, A, b, t, x[-1], eps)
        X.append(np.copy(x[-1]))
        T.append(len(x))
        D.append(d[0,0])
    return X, T, D

def plotMu(Q, p, A, b, X, Y, v0, eps, listMu):
    f = lambda x : x.T.dot(Q).dot(x)[0,0] + p.T.dot(x)[0,0]
    plt.figure()
    w = []
    d = []
    t = []
    for i in tqdm.tqdm(listMu):
        v, T, D = barr_method(Q, p, A, b, v0, eps, i)
        d.append(D)
        t.append(T)
        w.append(np.linalg.inv(X.T.dot(X)).dot(X.T).dot(v[-1]+Y))
        L = list(map(lambda x: f(x) - f(v[-1]), v))
        plt.plot(list(np.cumsum(np.array(T))), L, label = "mu = {}".format(i))
    plt.grid()
    plt.title(r"Influence of  $\mu$ over the log barrier resolution")
    plt.legend()
    plt.yscale('log')
    plt.xlabel("#Newton iterations")
    plt.ylabel(r"$f(v_t)-f^*$")
    plt.savefig('./fig/muInfluence.pdf')
    plt.show()
    plt.figure()
    for i in range(len(listMu)):
        T = t[i]
        plt.plot(t[i],[b.shape[0]/u for u in t[i]], label = "mu = {}".format(listMu[i]) )
    plt.grid()
    plt.title(r"Influence of $\mu$ over the precision criterion")
    plt.xlabel('#Newton iterations')
    plt.legend()
    plt.ylabel(r'Precision criterion $\frac{m}{t}$')
    plt.savefig('fig/precision2.pdf')
    plt.show()
    plt.figure()
    for i in range(len(listMu)):
        D = d[i]
        plt.plot(range(len(D)), D, label="mu = {}".format(listMu[i]))
    plt.grid()
    plt.title('Evolution of the precision criterion')
    plt.xlabel('#Iteration')
    plt.legend()
    plt.yscale('log')
    plt.ylabel(r'Newton decrement $\lambda^2$')
    plt.savefig('./fig/precision.pdf')
    plt.show()
    plt.figure()
    plt.plot(listMu, [u.T.dot(u)[0,0] for u in w])
    plt.grid()
    plt.title(r'Influence of $\mu$ over the norm' +'\n' + 'of optimal solution $w$ of the lasso problem')
    plt.xlabel("$mu$")
    plt.ylabel(r"$||w||^2$")
    plt.yscale("log")
    plt.savefig('./fig/optimal.pdf')
    plt.show()

def main():
    rd.seed(42)
    X, Y, lamb = genLassoProblem()
    Q, p, A, b = initProblem(X, Y, lamb)
    eps = 1e-4
    v0  = rd.rand(A.shape[1], 1)
    v0  = np.zeros((Q.shape[0],1))
    plotMu(Q, p, A, b, X, Y, v0, eps,[2, 5,15, 20, 50, 100, 200, 500, 1000, 2000])


if __name__=="__main__":
    main()
